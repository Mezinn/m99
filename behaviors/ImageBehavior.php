<?php

namespace app\behaviors;

use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use yii\web\UploadedFile;

class ImageBehavior extends Behavior {

    public $attribute;
    public $source;
    public $decorator;

    public function events() {
        return [
            BaseActiveRecord::EVENT_AFTER_VALIDATE => 'before_validate',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'refresh',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'refresh',
            BaseActiveRecord::EVENT_AFTER_FIND => 'after_find',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'after_delete',
        ];
    }

    public function before_validate() {
        $this->owner->{$this->attribute} = UploadedFile::getInstance($this->owner, $this->attribute);
    }

    public function refresh() {
        if ($this->owner->{$this->attribute}) {
            $this->owner->{$this->source} = $this->owner->{$this->source} ?: md5(time() . random_bytes(32)) .'.'. $this->owner->{$this->attribute}->getExtension();
            $this->owner->{$this->attribute}->saveAs($this->getPath() . $this->owner->{$this->source});
        }
    }

    public function after_find() {
        $this->owner->{$this->decorator} = $this->getPath() . $this->owner->{$this->source};
    }

    public function after_delete() {
        if (file_exists($this->getPath() . $this->owner->{$this->source}) and is_file($this->getPath() . $this->owner->{$this->source})) {
            unlink($this->getPath() . $this->owner->{$this->source});
        }
    }

    public function getPath() {
        return 'storage/images/';
    }

}
