<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $created_at
 * @property string $updated_at
 * @property string $auth_key
 * @property string $access_token
 *
 * @property FavoriteProducts[] $favoriteProducts
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface {

    public static function tableName() {
        return 'users';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public function rules() {
        return [
            [['username', 'password_hash', 'auth_key', 'access_token'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['username'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 64],
            [['auth_key', 'access_token'], 'string', 'max' => 32],
            [['username'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Ім\'я користувача',
            'password_hash' => 'Password Hash',
            'created_at' => 'Дата/час реєстрації',
            'updated_at' => 'Updated At',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteProducts() {
        return $this->hasMany(FavoriteProducts::className(), ['user_id' => 'id']);
    }

    public function getAuthKey(): string {
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey): bool {
        return $this->auth_key === $authKey;
    }

    public static function findIdentity($id): IdentityInterface {
        return self::findOne($id) ?: null;
    }

    public static function findIdentityByAccessToken($token, $type = null): IdentityInterface {
        return self::findOne(['access_token' => $token]) ?: null;
    }

    public function getFavoritesDataProvider() {
        return new \yii\data\ActiveDataProvider(['query' => FavoriteProducts::find()->where(['user_id' => $this->id])]);
    }

}
