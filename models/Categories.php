<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property int $is_active
 *
 * @property Products[] $products
 */
class Categories extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'categories';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    public function rules() {
        return [
            [['name', 'is_active'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['is_active'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'slug' => 'Slug',
            'created_at' => 'Дата/час створення',
            'updated_at' => 'Дата/час редагування',
            'is_active' => 'Активно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

}
