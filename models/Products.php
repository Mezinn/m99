<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use app\behaviors\ImageBehavior;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $name
 * @property int $category_id
 * @property string $price
 * @property string $description
 * @property int $preview_image_name
 * @property string $created_at
 * @property string $updated_at
 * @property int $is_active
 * @property string $slug
 * @property int $amount
 *
 * @property FavoriteProducts[] $favoriteProducts
 * @property Categories $category
 */
class Products extends \yii\db\ActiveRecord {

    public $preview_image_path;
    public $preview_image;

    public static function tableName() {
        return 'products';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
            'preview_image' => [
                'class' => ImageBehavior::className(),
                'attribute' => 'preview_image',
                'source' => 'preview_image_name',
                'decorator' => 'preview_image_path',
            ],
        ];
    }

    public function rules() {
        return [
            [['name', 'category_id', 'price', 'description', 'is_active', 'amount'], 'required'],
            [['category_id', 'is_active', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['preview_image_name'], 'string', 'max' => 64],
            [['price'], 'number'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['slug'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
            [['preview_image'], 'file'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'category_id' => 'Категорія',
            'price' => 'Ціна',
            'description' => 'Інформація',
            'preview_image_name' => 'Preview Image Name',
            'created_at' => 'Дата/час створення',
            'updated_at' => 'Дата/час редагування',
            'is_active' => 'Активно',
            'slug' => 'Slug',
            'amount' => 'Кількість',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteProducts() {
        return $this->hasMany(FavoriteProducts::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

}
