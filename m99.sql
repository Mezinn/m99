-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2018 at 12:05 PM
-- Server version: 10.0.36-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.2.11-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m99`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('ADMIN', '1', 1542095550),
('ADMIN', '5', 1542103379),
('ADMIN', '6', 1542103445),
('CUSTOMER', '3', 1542096029),
('CUSTOMER', '4', 1542102814);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('ADMIN', 1, NULL, NULL, NULL, 1542095383, 1542095383),
('CUSTOMER', 1, NULL, NULL, NULL, 1542095383, 1542095383);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Something', 'something', '2018-11-12 21:57:18', '2018-11-12 22:05:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `favorite_products`
--

CREATE TABLE `favorite_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1542095259),
('m140506_102106_rbac_init', 1542095263),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1542095263);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `preview_image_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `price`, `description`, `preview_image_name`, `created_at`, `updated_at`, `is_active`, `slug`, `amount`) VALUES
(1, 'Назва продуктуsadflmjbandsijfnadsfsdfnasddfnasdnfkasdfnasdfasdfasdfadfadsfafasdfasdfadsfawerafsD', 1, '5000.00', '555', '1e52a9757ee2f72ee98e6bc4b74d326b.jpg', '2018-11-12 22:09:03', '2018-11-13 08:48:15', 1, 'nazva-produktusadflmjbandsijfnadsfsdfnasddfnasdnfkasdfnasdfasdfasdfadfadsfafasdfasdfadsfawerafsd', 5000),
(2, 'Машина', 1, '225.00', 'Інформація', '618aeed88de173ac085f412622b52110.jpg', '2018-11-13 08:22:36', '2018-11-13 08:22:36', 1, 'masina', 555),
(3, 'ваапів', 1, '2345.00', 'івапівап', 'f705f05569d832b2a2f35eff2d173f5c.jpg', '2018-11-13 08:23:08', '2018-11-13 08:23:08', 1, 'vaapiv', 3452345),
(4, 'йцукйцукйцукйцукуцк', 1, '99999999.99', '1234123412', 'aa550f86f5c5bdb6672ea627a2f4dfa7.jpg', '2018-11-13 08:23:30', '2018-11-13 08:23:30', 1, 'jcukjcukjcukjcukuck', 1234),
(5, 'Lamont Gorczany', 1, '3000.00', 'Tempore consequatur reiciendis consequatur temporibus magnam. Unde rerum sit nobis est odio adipisci. Deserunt occaecati eum et.', NULL, '2018-11-13 08:29:08', '2018-11-13 08:29:08', 1, 'lamont-gorczany', 10),
(6, 'Kasandra Durgan', 1, '3000.00', 'In ex perspiciatis voluptatem voluptas est pariatur nemo fuga. Nostrum quisquam deserunt quia recusandae ea alias id. Sed fuga autem eaque rem. Et voluptas ut at ullam.', NULL, '2018-11-13 08:29:37', '2018-11-13 08:29:37', 1, 'kasandra-durgan', 10),
(7, 'Prof. Marge Stiedemann V', 1, '3000.00', 'Voluptatem accusamus labore autem ex ut. Nihil sed laboriosam omnis. Nemo quam et laudantium similique officiis ducimus.', NULL, '2018-11-13 08:29:37', '2018-11-13 08:29:37', 1, 'prof-marge-stiedemann-v', 10),
(8, 'Wilfrid Swaniawski', 1, '3000.00', 'Omnis cum eum voluptatum sapiente doloribus pariatur cumque. Architecto molestiae et nihil incidunt sed occaecati aspernatur sint. A id quia maiores voluptatum magni non doloremque.', NULL, '2018-11-13 08:29:37', '2018-11-13 08:29:37', 1, 'wilfrid-swaniawski', 10),
(9, 'Dr. Osborne Schuster', 1, '3000.00', 'Et fuga necessitatibus et beatae ut qui. Odio unde et quaerat sint possimus soluta et. Velit debitis dolor est et ipsum quaerat. Qui in nisi quia.', NULL, '2018-11-13 08:29:37', '2018-11-13 08:29:37', 1, 'dr-osborne-schuster', 10),
(10, 'Tracey Veum', 1, '3000.00', 'Magni consequuntur sed voluptatum culpa eius. Eum impedit doloremque qui quo quo aut ut. Dolorem illo placeat eum. Repellat quaerat officia architecto.', NULL, '2018-11-13 08:29:37', '2018-11-13 08:29:37', 1, 'tracey-veum', 10),
(11, 'Rhianna Franecki III', 1, '3000.00', 'Qui sit aut consequatur aut facilis. Amet eos aut necessitatibus in distinctio ullam totam. Autem quidem consequuntur doloribus repellat. Ut eum ullam possimus ut quia.', NULL, '2018-11-13 08:29:37', '2018-11-13 08:29:37', 1, 'rhianna-franecki-iii', 10),
(12, 'Seamus Larson', 1, '3000.00', 'Ea est amet corporis ut. Omnis et dicta vitae voluptatem.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'seamus-larson', 10),
(13, 'Garnet Stark Jr.', 1, '3000.00', 'Animi corrupti laudantium necessitatibus velit ducimus. Odio eos et alias dolores sunt voluptate. Qui tenetur sit quaerat veritatis eveniet.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'garnet-stark-jr', 10),
(14, 'John Bogan', 1, '3000.00', 'Autem recusandae fuga et dignissimos voluptatibus et. Facilis repellendus quo facilis ratione explicabo vel non. Et consequatur quaerat sed quidem rem in. Unde totam corrupti officia voluptas ab.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'john-bogan', 10),
(15, 'Mrs. Tiffany Thiel', 1, '3000.00', 'At sed pariatur mollitia odit totam porro. Assumenda rerum sequi dolor expedita nam.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'mrs-tiffany-thiel', 10),
(16, 'Dr. Ned Rosenbaum IV', 1, '3000.00', 'Quo esse et qui nostrum dolor voluptate. Adipisci fuga odit est et. Illo et eos quam rerum quibusdam. Qui quae perferendis optio fuga voluptates itaque ea.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'dr-ned-rosenbaum-iv', 10),
(17, 'Joshua Schaefer', 1, '3000.00', 'Distinctio quaerat odit esse dolore natus. Est commodi doloribus cumque. Omnis at asperiores quos omnis omnis. Necessitatibus sed ut est iste.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'joshua-schaefer', 10),
(18, 'Amber Bogan', 1, '3000.00', 'Cum ea aperiam in est nihil et eaque. Mollitia aspernatur aut laudantium impedit quo. Et dignissimos ut et qui provident nam. Ut cupiditate deleniti magnam eligendi necessitatibus voluptatem est.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'amber-bogan', 10),
(19, 'Aimee Schaden', 1, '3000.00', 'Accusantium eius cupiditate eveniet eligendi qui rem. Modi vero ab quia est mollitia facere. Qui occaecati omnis deleniti rerum at ut.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'aimee-schaden', 10),
(20, 'Eveline Hintz', 1, '3000.00', 'Reiciendis ea vitae quae non ducimus minima. Modi repellat possimus ea voluptatibus. Sint nostrum in maiores sed. Voluptates porro perspiciatis earum magni.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'eveline-hintz', 10),
(21, 'Brown Sipes Jr.', 1, '3000.00', 'Ducimus incidunt expedita dolorem aperiam. Doloremque rerum veniam autem. Eaque maxime et in iste. Nisi quidem et at dolorem excepturi ex.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'brown-sipes-jr', 10),
(22, 'Dr. Wilson Donnelly PhD', 1, '3000.00', 'Ea quia corrupti quod et culpa similique adipisci. Quidem voluptatem modi autem est voluptatem. Facilis rerum rerum cumque qui. Inventore in enim minima labore ea.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'dr-wilson-donnelly-phd', 10),
(23, 'Roosevelt Mills', 1, '3000.00', 'Nobis ipsum occaecati nisi. Quia deserunt distinctio magnam eius non dolorum est. Sapiente velit exercitationem consequatur amet. Quidem ut quia corporis autem minima beatae vel.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'roosevelt-mills', 10),
(24, 'Prof. Concepcion Upton', 1, '3000.00', 'Aperiam minima exercitationem odit aut. Eum debitis optio temporibus enim. Laborum sed sint voluptatem tempora aspernatur quod. Voluptatem beatae quae corrupti qui quos dolorum.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'prof-concepcion-upton', 10),
(25, 'Mrs. Hanna Wolff I', 1, '3000.00', 'Id consequatur occaecati nisi pariatur voluptatem in quae. Magnam et qui veritatis et vel autem. Omnis vel quas et voluptas non aut.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'mrs-hanna-wolff-i', 10),
(26, 'Novella Hyatt III', 1, '3000.00', 'Placeat vero molestiae ad quo recusandae. Ut adipisci exercitationem dolorem eligendi sapiente voluptatem.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'novella-hyatt-iii', 10),
(27, 'Dr. Alta Stiedemann', 1, '3000.00', 'Saepe qui voluptatem est dolorum voluptas ducimus eius. Ea ut sed nesciunt. Qui dolorum consequatur et explicabo fuga. Non consequuntur odit et consequuntur.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'dr-alta-stiedemann', 10),
(28, 'Britney Walter', 1, '3000.00', 'Vero beatae fugit repudiandae voluptatum quidem illum dolores eaque. Qui eos aliquam quam odit. Occaecati consequuntur provident nobis officiis. Sit est optio quasi.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'britney-walter', 10),
(29, 'Jaydon Haley IV', 1, '3000.00', 'Neque ipsa in sunt quis saepe rerum et. Id inventore veniam quidem sed vero hic blanditiis. Sint beatae minus illum adipisci ut.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'jaydon-haley-iv', 10),
(30, 'Dr. Carlo Pouros', 1, '3000.00', 'Dicta non quam aliquid ab quam. Dicta veritatis deleniti quis quia recusandae veritatis. Consequatur impedit ut odio ut autem non. Ducimus iusto fuga labore.', NULL, '2018-11-13 08:29:38', '2018-11-13 08:29:38', 1, 'dr-carlo-pouros', 10),
(31, 'Amelie Haley V', 1, '3000.00', 'A accusamus eum voluptas. Laboriosam saepe ipsum harum ab et perferendis voluptas earum. Dolores voluptatem consectetur suscipit laudantium ut expedita voluptate quia.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'amelie-haley-v', 10),
(32, 'Prof. Thaddeus Schmeler', 1, '3000.00', 'Non id quod itaque a officiis. Autem voluptatem nobis voluptatum sed tempore. Totam commodi est natus porro in fugiat. Accusamus enim omnis voluptates impedit.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'prof-thaddeus-schmeler', 10),
(33, 'Mr. Aiden Denesik I', 1, '3000.00', 'Unde officiis id consequatur enim voluptatem in. Qui eaque id hic nihil quia inventore eligendi. Et autem incidunt eligendi non. Asperiores ut quia fuga aut laboriosam nisi a perferendis.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'mr-aiden-denesik-i', 10),
(34, 'Tierra Koelpin', 1, '3000.00', 'Voluptatum nesciunt qui officia sunt odio et id. Ut voluptate alias quidem quidem. Eaque et et sit tenetur nobis pariatur. Et consequatur beatae vel enim nihil qui.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'tierra-koelpin', 10),
(35, 'Rosamond Hill', 1, '3000.00', 'Est cumque et molestiae quas accusamus libero deserunt. Consequuntur sit recusandae atque laudantium. Unde et quasi nemo ipsam non error sit.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'rosamond-hill', 10),
(36, 'Dr. Assunta Tromp DVM', 1, '3000.00', 'Exercitationem neque vitae magnam recusandae cum. Eum aut sit blanditiis occaecati. Laudantium laborum velit exercitationem quasi dolore sit. Cumque dignissimos error non voluptas.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'dr-assunta-tromp-dvm', 10),
(37, 'Mrs. Carrie Mayert II', 1, '3000.00', 'Quis nobis excepturi qui eum similique. Omnis nisi aut et doloremque itaque enim rerum et.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'mrs-carrie-mayert-ii', 10),
(38, 'Willard Orn', 1, '3000.00', 'Repellat dolores repellat eligendi et illum eligendi. Corrupti non sequi enim consectetur aut. Qui dolorum veritatis laudantium alias qui fugiat est.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'willard-orn', 10),
(39, 'Prof. Karianne Larson I', 1, '3000.00', 'Earum et fugit at quos illum et ut. Accusamus nihil sint voluptatum est tempora aut voluptas. Praesentium voluptatem et sit natus. Sit facilis pariatur sint hic.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'prof-karianne-larson-i', 10),
(40, 'Stan Mosciski', 1, '3000.00', 'Inventore magnam accusamus aliquid odio repellendus aspernatur. Mollitia impedit qui dolores voluptas ut sit ex. Non veniam in commodi et excepturi vel fugit.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'stan-mosciski', 10),
(41, 'Hadley Gibson', 1, '3000.00', 'Veritatis ratione quo enim sequi consequatur hic commodi. Odio ipsam aut est. Quis sit voluptas consequatur temporibus. Et autem optio eum quo rem explicabo.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'hadley-gibson', 10),
(42, 'Prof. Brenna O\'Kon IV', 1, '3000.00', 'Hic ullam aspernatur dolorum eos. Praesentium non distinctio nemo quia et molestiae velit voluptas.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'prof-brenna-okon-iv', 10),
(43, 'Jessika King', 1, '3000.00', 'Vel voluptatibus excepturi tenetur eum eveniet ipsa. Laudantium aut ipsam et nam id aliquid. Suscipit quam aperiam distinctio aliquid. Molestias harum aut quisquam voluptate.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'jessika-king', 10),
(44, 'Sharon Dickens', 1, '3000.00', 'Ea maxime nobis est veniam eius nostrum eveniet. Maxime qui sit sed dolore. Minus consequatur qui quia doloremque ut quis.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'sharon-dickens', 10),
(45, 'Nicholaus Jenkins', 1, '3000.00', 'Consequuntur dolorem explicabo laboriosam numquam perferendis. Nulla cupiditate error voluptatem sunt. Molestias autem ad blanditiis quia dolore.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'nicholaus-jenkins', 10),
(46, 'Calista Schneider', 1, '3000.00', 'Quia blanditiis veniam sequi rerum. Id repudiandae totam eum sed nostrum cumque saepe.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'calista-schneider', 10),
(47, 'Gaylord Mraz', 1, '3000.00', 'Debitis dolores harum officiis facilis aliquid eum mollitia. Nihil numquam quas expedita ut fugiat est. Quidem eius natus dolores nam.', NULL, '2018-11-13 08:29:39', '2018-11-13 08:29:39', 1, 'gaylord-mraz', 10),
(48, 'Prof. Johann DuBuque', 1, '3000.00', 'Quae ipsam nulla quis veritatis. Ut similique ad qui sunt adipisci. Velit optio aut quia. Quis veniam voluptatem occaecati non.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'prof-johann-dubuque', 10),
(49, 'Clementine Grady', 1, '3000.00', 'Sunt qui facilis earum maiores enim aut vel. Voluptates ex illo neque eum aliquam delectus sed exercitationem. Sequi sit a earum quo dolor enim qui dignissimos.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'clementine-grady', 10),
(50, 'Frederique McClure', 1, '3000.00', 'Sed aut aliquid aut quia et. At facilis vitae sit voluptate corporis. Amet neque vitae nesciunt. Autem dolorem velit sit maxime ipsum aperiam quisquam.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'frederique-mcclure', 10),
(51, 'Mr. Fern Will II', 1, '3000.00', 'Molestiae natus nobis molestias possimus impedit ipsum. Deleniti voluptatem est ut. Necessitatibus laudantium optio eum atque.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'mr-fern-will-ii', 10),
(52, 'Keira Sawayn', 1, '3000.00', 'Sint dolorum corrupti vitae pariatur. Blanditiis sit iusto debitis voluptatem quisquam dolor. Illo architecto vero est earum earum cum deleniti ipsam. Quasi dolores dolores quibusdam a.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'keira-sawayn', 10),
(53, 'Helga Rau III', 1, '3000.00', 'Animi voluptate laboriosam eum et facilis. Blanditiis commodi est aut repellendus a minima consectetur sapiente.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'helga-rau-iii', 10),
(54, 'Prof. Jaycee Moore II', 1, '3000.00', 'Ut sed quia ut et sint cupiditate. Fugiat vero quia dolore facilis. Magni repellat in dignissimos dolore consequatur et.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'prof-jaycee-moore-ii', 10),
(55, 'Gino Rau', 1, '3000.00', 'Delectus aut facilis quisquam illo mollitia suscipit. At eveniet sed ea alias reiciendis. Sint totam eos neque et culpa optio.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'gino-rau', 10),
(56, 'Reanna Sauer', 1, '3000.00', 'Id dolorem vel magnam optio aut error. Repellendus cumque aut atque est necessitatibus. Quia iure vel exercitationem occaecati eveniet eos odit.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'reanna-sauer', 10),
(57, 'Mrs. Emmanuelle Rosenbaum', 1, '3000.00', 'Eligendi voluptatem laborum inventore quia consectetur. Cupiditate excepturi ut nisi dolore. Omnis ipsam assumenda modi at dolorum nisi ut. Atque architecto sit est eius voluptate hic quis eaque.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'mrs-emmanuelle-rosenbaum', 10),
(58, 'Sandrine Bailey', 1, '3000.00', 'Vel dolorum dicta sint incidunt maiores. Explicabo rem ducimus et quisquam doloremque doloremque. Enim quae voluptatem in illo doloremque reiciendis quo magni. Amet molestias qui doloribus aut.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'sandrine-bailey', 10),
(59, 'Amina Fahey', 1, '3000.00', 'Ex eius quia sed. Et a animi est voluptatem aut ullam.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'amina-fahey', 10),
(60, 'Kaya Pouros', 1, '3000.00', 'Velit a asperiores rerum assumenda aspernatur repudiandae amet. Necessitatibus et dignissimos rerum qui praesentium.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'kaya-pouros', 10),
(61, 'Mrs. Roxanne O\'Connell Jr.', 1, '3000.00', 'Dolores aut in voluptas sit. Velit est quia est. Fuga ut et et facilis.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'mrs-roxanne-oconnell-jr', 10),
(62, 'Dr. Tressie Funk V', 1, '3000.00', 'Aperiam tenetur nihil est dolores. Libero voluptatem corrupti necessitatibus dolores consequatur vero rerum. Sunt natus ut ea. Impedit eos consequatur in suscipit sunt tenetur.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'dr-tressie-funk-v', 10),
(63, 'Miss Euna Cummings', 1, '3000.00', 'Delectus consequuntur reprehenderit esse fuga eum amet. Voluptatem asperiores omnis aliquam cumque. Occaecati inventore commodi similique odit.', NULL, '2018-11-13 08:29:40', '2018-11-13 08:29:40', 1, 'miss-euna-cummings', 10),
(64, 'Lorenza Satterfield Jr.', 1, '3000.00', 'Consequatur vel fugit repudiandae maiores eos eum et. Autem autem et non et ut. Eligendi ipsam est nostrum non rerum.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'lorenza-satterfield-jr', 10),
(65, 'Marlee Harber', 1, '3000.00', 'Iure et necessitatibus suscipit hic est sunt eos laborum. Provident cupiditate atque minus asperiores quasi animi. In nesciunt eos commodi eum. Rerum ratione nemo deleniti porro et et.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'marlee-harber', 10),
(66, 'Zack Sporer', 1, '3000.00', 'Aspernatur consequatur non dolorem ut omnis non perferendis qui. Non neque quaerat tempora laborum. Sequi qui ut consectetur est et est.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'zack-sporer', 10),
(67, 'Kay Rodriguez', 1, '3000.00', 'In aut iste ea sed libero quisquam dolor. Quis eaque repudiandae aut sed. Ut repellendus ipsum et deleniti. Quidem sunt excepturi ab similique eum nisi et quos.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'kay-rodriguez', 10),
(68, 'Dr. Kiley Upton', 1, '3000.00', 'Nobis et consequatur recusandae similique quos. Sit aliquam culpa ipsam magnam rerum praesentium error placeat. Nam doloremque est odio inventore.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'dr-kiley-upton', 10),
(69, 'Dr. Brenna Cormier', 1, '3000.00', 'Id corporis amet similique sunt eos possimus eligendi. Repudiandae recusandae expedita ex esse. Aliquam nihil quod quae ex quam.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'dr-brenna-cormier', 10),
(70, 'Earlene Haag II', 1, '3000.00', 'Saepe perspiciatis consequatur quod eaque reiciendis. Vero explicabo voluptatem dolore quod quia. Nemo ad delectus recusandae odit magnam. Delectus quod et nam optio non est adipisci aut.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'earlene-haag-ii', 10),
(71, 'Amy Simonis III', 1, '3000.00', 'Alias cupiditate minus maxime consequatur illo consequatur autem. Amet exercitationem sit saepe id voluptas. Molestiae tempora dolores ut.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'amy-simonis-iii', 10),
(72, 'Dr. Trey Spencer DDS', 1, '3000.00', 'Quas nam eligendi porro et cumque impedit perspiciatis nihil. Voluptates excepturi rerum illum dolorem quidem architecto aut. Blanditiis voluptatum hic non voluptas natus modi.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'dr-trey-spencer-dds', 10),
(73, 'Prof. Jake Gerlach MD', 1, '3000.00', 'Iusto voluptatem sequi dolor et quibusdam in. Iure excepturi in repellat animi ipsum veritatis qui.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'prof-jake-gerlach-md', 10),
(74, 'Lulu Simonis', 1, '3000.00', 'Vitae molestias facere dolor quod atque vitae quia veniam. Odit sunt et totam. Dignissimos et eius qui.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'lulu-simonis', 10),
(75, 'Ms. Marilyne Gulgowski', 1, '3000.00', 'Necessitatibus ipsam alias fugit. Nihil id dignissimos iusto ad. Iure architecto libero dolore.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'ms-marilyne-gulgowski', 10),
(76, 'Savion Lang', 1, '3000.00', 'Odio dolorum amet corporis non. Sed in quibusdam laudantium sunt non. Nulla ut iusto in ipsum deleniti autem. Molestias cumque esse doloremque totam quo aut illo.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'savion-lang', 10),
(77, 'Dr. Elian Bosco DVM', 1, '3000.00', 'Reprehenderit non sit aliquid et sed ea. Temporibus iure nihil maiores exercitationem.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'dr-elian-bosco-dvm', 10),
(78, 'Abigale Cronin', 1, '3000.00', 'Excepturi deleniti quidem est vel ut quis. Eum vel aut culpa odit. Minus deserunt quia distinctio mollitia ut voluptatum saepe. Nemo exercitationem laborum rem illum asperiores nisi ad.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'abigale-cronin', 10),
(79, 'Prof. Wendell Wisozk DVM', 1, '3000.00', 'Est accusantium est error at minima autem fuga. Quo et id velit qui architecto dolorum doloribus. In optio molestiae earum facilis.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'prof-wendell-wisozk-dvm', 10),
(80, 'Lora Hegmann', 1, '3000.00', 'Inventore voluptatum tempora enim a id. Quo odio doloremque error rem harum. Et mollitia dolor dicta architecto laudantium veniam nemo. Porro alias esse delectus est.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'lora-hegmann', 10),
(81, 'Salma Nolan', 1, '3000.00', 'Voluptas aut repellat error recusandae ullam incidunt. Voluptas dolorem ut iste rerum. Harum occaecati ducimus ducimus fugiat doloribus. Doloremque voluptas occaecati magnam atque veniam.', NULL, '2018-11-13 08:29:41', '2018-11-13 08:29:41', 1, 'salma-nolan', 10),
(82, 'Alaina Schumm', 1, '3000.00', 'Soluta et autem possimus optio. Quo porro ut et. Dolore eum placeat enim facilis ut et. Alias quia harum dolorem et necessitatibus consequatur qui eos.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'alaina-schumm', 10),
(83, 'Dahlia Schaden', 1, '3000.00', 'Itaque qui magni et omnis expedita. Quae repellat et error ut delectus modi. Quisquam voluptatem cumque sequi ipsum debitis. Quia reprehenderit facilis alias.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'dahlia-schaden', 10),
(84, 'Miss Lacey Weimann Sr.', 1, '3000.00', 'Sit qui qui maxime. Culpa est iste esse delectus omnis magni tempora. Nisi mollitia saepe ipsum dolor incidunt. Eveniet quibusdam corrupti dolorum quasi.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'miss-lacey-weimann-sr', 10),
(85, 'Jonathan Brown', 1, '3000.00', 'Ipsum quas quae blanditiis ipsum sunt. Aut recusandae laborum sequi excepturi dolorem rerum. Illum voluptatem et aut.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'jonathan-brown', 10),
(86, 'Retta Beatty III', 1, '3000.00', 'Maxime amet animi ducimus reprehenderit. Quis eos ipsum debitis rerum repudiandae cumque doloribus. Corporis quam qui nesciunt ducimus rem. Quas aliquam beatae qui quis.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'retta-beatty-iii', 10),
(87, 'Robyn Runolfsson', 1, '3000.00', 'Officiis doloribus autem ab consequatur quaerat voluptates. Non quis aspernatur eveniet eveniet. Animi quo iure numquam corrupti.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'robyn-runolfsson', 10),
(88, 'Houston Will II', 1, '3000.00', 'Molestiae laboriosam voluptatem qui eos asperiores dolores est. Pariatur est molestiae et odit nisi. Explicabo qui quidem odio aut laboriosam quos reiciendis.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'houston-will-ii', 10),
(89, 'Norberto Rau', 1, '3000.00', 'Est nostrum voluptates non qui sunt harum qui ut. Consectetur perferendis ratione sit ut beatae aut cupiditate. Est quisquam quis id laboriosam voluptatem non.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'norberto-rau', 10),
(90, 'Paige Lockman', 1, '3000.00', 'Iure et officia quibusdam at ipsa explicabo. Est repudiandae qui doloribus et vel ut dolorem autem. Enim excepturi esse animi cumque rerum iste.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'paige-lockman', 10),
(91, 'Prof. Nick Harris', 1, '3000.00', 'Modi molestias eos labore accusantium est sed placeat. Est quidem quasi facere aut. Voluptatibus delectus nulla ut beatae occaecati id tenetur.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'prof-nick-harris', 10),
(92, 'Wilma Quigley', 1, '3000.00', 'Incidunt non quia corporis fugiat aut unde. Est minus hic rerum sit tempore. Eaque voluptas vero et ipsam temporibus assumenda excepturi.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'wilma-quigley', 10),
(93, 'Jayce Sipes', 1, '3000.00', 'Deleniti quisquam odio omnis magnam rem dolorum. Adipisci est ipsum necessitatibus nam quos. Quas aperiam consectetur totam laborum. Ipsam ut et vel aperiam. Aliquid quia facilis nihil autem nostrum.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'jayce-sipes', 10),
(94, 'Polly Hyatt', 1, '3000.00', 'Rerum perspiciatis fuga doloribus at. Deleniti earum et aliquid dolorem provident ipsa eligendi. Et perferendis itaque quo cum.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'polly-hyatt', 10),
(95, 'Willow Gibson I', 1, '3000.00', 'Voluptatum neque minus optio eos. Aut sed et repellendus consequatur iste et. Iure et quasi odio accusamus quos. Quas id aut molestiae omnis ut quisquam. Voluptatum ducimus numquam repudiandae.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'willow-gibson-i', 10),
(96, 'Sydney Hamill DVM', 1, '3000.00', 'Quos et quia quia et impedit ratione. Enim rerum porro quidem perspiciatis ducimus. Nemo laborum tenetur impedit voluptates quis cumque officiis. Sunt temporibus dolore est recusandae ex.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'sydney-hamill-dvm', 10),
(97, 'Mattie Emmerich', 1, '3000.00', 'Autem et eum dignissimos dolorem sed. Consequatur officia quia quaerat ipsa nam. Quis aperiam culpa minus itaque quaerat architecto similique.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'mattie-emmerich', 10),
(98, 'Rosa Lehner', 1, '3000.00', 'Labore voluptas qui id qui distinctio et. Voluptatibus provident voluptatibus beatae sed. Possimus praesentium ad cupiditate asperiores.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'rosa-lehner', 10),
(99, 'Prof. Loraine Bogisich', 1, '3000.00', 'Et ut laudantium veniam excepturi. Qui rerum eligendi tenetur repellat quasi.', NULL, '2018-11-13 08:29:42', '2018-11-13 08:29:42', 1, 'prof-loraine-bogisich', 10),
(100, 'Dr. Madyson Tremblay III', 1, '3000.00', 'Voluptas dolorum vero architecto quia sit error eius. Qui molestiae aut voluptas consectetur. Consequuntur esse nemo et.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'dr-madyson-tremblay-iii', 10),
(101, 'Mr. Adonis Weber DDS', 1, '3000.00', 'Commodi officiis eveniet fuga hic corrupti. Nesciunt aut facere ea non qui aut eum. Doloribus alias molestiae dolorem. Aut laudantium numquam veniam asperiores soluta reprehenderit quas.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'mr-adonis-weber-dds', 10),
(102, 'Isaiah Windler', 1, '3000.00', 'Vitae accusamus repudiandae quo hic. Quam sed alias consequatur velit. Et voluptatem enim consequatur quasi.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'isaiah-windler', 10),
(103, 'Prof. Leanne Lindgren', 1, '3000.00', 'Eius vel placeat iste voluptatem quasi. Et soluta ipsa dignissimos nam veniam. Optio aut enim cupiditate repudiandae doloribus. Mollitia accusantium temporibus provident repellendus rem saepe.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'prof-leanne-lindgren', 10),
(104, 'Darby Sauer Jr.', 1, '3000.00', 'Qui totam porro assumenda possimus et deserunt. Ratione fugiat explicabo et reiciendis harum at iste et. Quia autem sapiente libero fuga.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'darby-sauer-jr', 10),
(105, 'Yoshiko Reilly', 1, '3000.00', 'Laboriosam magni asperiores laborum et. Eum id velit sequi optio eum. Ratione a atque error voluptatibus. Cum adipisci nemo vitae reiciendis tenetur qui est qui.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'yoshiko-reilly', 10),
(106, 'Sherwood Schultz IV', 1, '3000.00', 'Rerum quae inventore pariatur. Ut harum ut veniam eum illum culpa dolor.', NULL, '2018-11-13 08:29:43', '2018-11-13 08:29:43', 1, 'sherwood-schultz-iv', 10),
(107, 'Ms. Zoila Kub MD', 1, '3000.00', 'Sit provident commodi animi magni dolorem qui aliquid. Temporibus et consequuntur voluptatem est voluptatem vero.', NULL, '2018-11-13 09:51:10', '2018-11-13 09:51:10', 1, 'ms-zoila-kub-md', 10),
(108, 'Angelita Murazik', 1, '3000.00', 'Iusto voluptas non voluptas excepturi at molestias. Et quae facere qui quibusdam maiores odit fuga. Ducimus molestiae esse officia consequatur vel aut.', NULL, '2018-11-13 09:51:10', '2018-11-13 09:51:10', 1, 'angelita-murazik', 10),
(109, 'Mrs. Willa Ryan PhD', 1, '3000.00', 'Facere asperiores debitis eaque quia similique numquam molestiae sequi. Velit est quam est eos reiciendis. Deserunt totam consectetur molestiae qui sint.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'mrs-willa-ryan-phd', 10),
(110, 'Aditya Kreiger', 1, '3000.00', 'Ut molestias velit odit magni natus iure necessitatibus minima. Corrupti quaerat dolor ipsa eveniet.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'aditya-kreiger', 10),
(111, 'Mr. Sydney Ledner', 1, '3000.00', 'Quia dolor possimus aliquam et soluta tempora. Sit facilis qui dolore autem asperiores vero.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'mr-sydney-ledner', 10),
(112, 'Mrs. Aryanna Cummings IV', 1, '3000.00', 'Quis est nesciunt quia in rerum corporis. Numquam exercitationem aspernatur libero harum. Minus commodi incidunt aut.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'mrs-aryanna-cummings-iv', 10),
(113, 'Chaz Bogan', 1, '3000.00', 'Quam quia omnis provident non perferendis. Asperiores error quisquam quidem consequatur. Eius quaerat et in optio voluptatem itaque et. Est aliquid nobis quia qui quia saepe.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'chaz-bogan', 10),
(114, 'Davion Kertzmann', 1, '3000.00', 'Iste voluptas ut explicabo molestiae quo ut sint. Temporibus repellat est nesciunt amet. Asperiores porro amet similique in maxime natus aut.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'davion-kertzmann', 10),
(115, 'Mrs. Marquise Lowe', 1, '3000.00', 'Aspernatur provident eos voluptas. Officiis officia ratione rerum voluptatibus vero ut ab. Vel debitis labore quia eius. Repellendus incidunt sequi soluta deleniti doloribus maxime earum quia.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'mrs-marquise-lowe', 10),
(116, 'Mrs. Karlie Walter IV', 1, '3000.00', 'Fugiat qui voluptatum necessitatibus vitae placeat totam. Eum qui blanditiis sint nobis fuga repellendus et deserunt. At ut dignissimos aperiam. Sit numquam eum vero dolore non velit numquam.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'mrs-karlie-walter-iv', 10),
(117, 'Shania Buckridge', 1, '3000.00', 'Eos dolores nobis dolor aut. Et sed laboriosam exercitationem voluptatem. Non laudantium ex provident aliquam incidunt impedit.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'shania-buckridge', 10),
(118, 'George Stroman', 1, '3000.00', 'Explicabo saepe qui quia quam labore quia. Et architecto cupiditate doloremque neque error dolorum earum. Enim nihil autem placeat quia sed.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'george-stroman', 10),
(119, 'Dagmar Gleason', 1, '3000.00', 'Et saepe dignissimos iure aliquam facilis. Eos accusamus ut suscipit. Et possimus porro ratione modi beatae. Placeat pariatur maxime enim veritatis quisquam fuga.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'dagmar-gleason', 10),
(120, 'Geoffrey Heidenreich', 1, '3000.00', 'Necessitatibus exercitationem inventore earum ea consequatur. Et dicta odio corrupti dolore. Modi ut sed et fugit culpa totam in aut.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'geoffrey-heidenreich', 10),
(121, 'Colten Zieme', 1, '3000.00', 'Quidem natus at quos occaecati voluptatem voluptatum. Ea eos et et ratione aspernatur cupiditate. Id mollitia aut magnam modi est.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'colten-zieme', 10),
(122, 'Ceasar Sawayn', 1, '3000.00', 'Voluptatum eos molestiae quae aut blanditiis velit. Exercitationem voluptatem quia facere et labore. Mollitia et autem provident. Ut eum occaecati sint esse.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'ceasar-sawayn', 10),
(123, 'Reilly Collins', 1, '3000.00', 'Et eius neque enim doloremque sit ut culpa. Eius dolor rerum illo fugit id. Consequuntur omnis deserunt hic sed aperiam omnis. Ea sed quo et quis enim ut.', NULL, '2018-11-13 09:51:11', '2018-11-13 09:51:11', 1, 'reilly-collins', 10),
(124, 'Dr. Hubert Labadie Jr.', 1, '3000.00', 'Nihil hic facere autem quam omnis autem. Ad aliquid reiciendis officia et recusandae. Voluptas sapiente et deleniti molestiae non necessitatibus hic.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'dr-hubert-labadie-jr', 10),
(125, 'Markus Kunde', 1, '3000.00', 'Eum id porro earum id sint qui. Aut eum quos vel quibusdam et minus. Nesciunt quaerat quas fugit ratione hic et aut. Voluptates repellat vitae eum rerum.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'markus-kunde', 10),
(126, 'Miss Robyn Hammes', 1, '3000.00', 'Doloribus culpa delectus eveniet dolorem blanditiis. Officiis laborum corporis dolores non magnam sit modi ut. Ad velit dicta magni et est officiis cum.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'miss-robyn-hammes', 10),
(127, 'Dr. Shany Becker', 1, '3000.00', 'Facere et non hic officiis in. Amet ut ut soluta a. Voluptas quae beatae ipsa harum non cumque. Est est consequatur dolorem aspernatur.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'dr-shany-becker', 10),
(128, 'Sydnee Schuster', 1, '3000.00', 'Amet vel est quasi repellat ducimus sunt sunt. Rerum et id qui enim vitae est corrupti voluptates.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'sydnee-schuster', 10),
(129, 'Erick Jerde', 1, '3000.00', 'Quae dolorum illo exercitationem et hic. Incidunt eum quam quaerat suscipit. Quod corporis porro ut qui laborum. Consequatur in voluptatum rerum ea modi.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'erick-jerde', 10),
(130, 'Bridie Wunsch I', 1, '3000.00', 'Repellendus eos deleniti nobis alias. Ipsam qui maxime delectus ea vel odio est quo. Quibusdam cum exercitationem dolorum est.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'bridie-wunsch-i', 10),
(131, 'Frieda Walsh', 1, '3000.00', 'Fugiat id facere aperiam vitae unde. Et fugiat modi velit qui pariatur aut itaque. Nam voluptatem voluptatem qui deleniti maxime consequatur. Molestias sit eum eaque ipsam ullam ut.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'frieda-walsh', 10),
(132, 'Kelly O\'Reilly', 1, '3000.00', 'Sint non quidem aut. Vel odit minus natus enim consequatur beatae totam. Possimus temporibus est delectus impedit illum quam distinctio velit. Et dicta iure et aut vel et.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'kelly-oreilly', 10),
(133, 'Prof. Emily Schowalter IV', 1, '3000.00', 'Consequatur architecto tempora quibusdam omnis voluptas. Rerum id error qui nemo. Et et non expedita fuga et sapiente.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'prof-emily-schowalter-iv', 10),
(134, 'Dr. Shaniya Ullrich DVM', 1, '3000.00', 'Voluptatibus dicta saepe et nemo numquam voluptates consequuntur. Maiores aut molestiae cumque sit repudiandae. Enim quas nihil eius architecto. Officia velit quis non ipsum est.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'dr-shaniya-ullrich-dvm', 10),
(135, 'Orville Robel', 1, '3000.00', 'Provident illo ut sequi nulla eum aliquam consequatur. Ipsum nisi rem veniam doloribus.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'orville-robel', 10),
(136, 'Shemar Rosenbaum', 1, '3000.00', 'Et eaque sed totam ipsam error. Quis cupiditate vitae nihil consequatur. Nulla rerum soluta vero accusantium velit dicta.', NULL, '2018-11-13 09:51:12', '2018-11-13 09:51:12', 1, 'shemar-rosenbaum', 10),
(137, 'Dr. Marion McCullough II', 1, '3000.00', 'Omnis tempora dolor ex rem id. Sed ipsum qui perferendis eius fuga. Sit voluptatem eum soluta et. Qui vel sequi eaque aut. Qui beatae fugit dolore. Tempora repudiandae at quam iure ea minima.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'dr-marion-mccullough-ii', 10),
(138, 'Kennedy Pacocha V', 1, '3000.00', 'Iusto reiciendis nulla est unde aut animi. Vero officiis quo eos et.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'kennedy-pacocha-v', 10),
(139, 'Prof. Ubaldo Bogisich Jr.', 1, '3000.00', 'Nihil optio exercitationem dolorum sint veniam consequatur cum. Officia voluptatum totam molestiae expedita dolores et ipsum alias. Distinctio sit quasi reiciendis nam eveniet modi est.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'prof-ubaldo-bogisich-jr', 10),
(140, 'Ms. Ollie Mills', 1, '3000.00', 'Aliquid consequuntur voluptatibus in nisi voluptas voluptas quae consectetur. Non et nam labore sit expedita eius.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'ms-ollie-mills', 10),
(141, 'Euna Predovic', 1, '3000.00', 'Nobis et dolorum sit quia nisi. Est corrupti reiciendis omnis dolor numquam quas. Iure nihil et omnis amet. Omnis quas qui illo et. Aut ut ut aut et. Dolorum exercitationem qui corrupti et odit.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'euna-predovic', 10),
(142, 'Aaron Kuhic DVM', 1, '3000.00', 'In nihil animi id commodi vitae sequi. Enim dignissimos necessitatibus commodi. Et eum consequatur et amet nihil amet. Aut repellat quisquam esse.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'aaron-kuhic-dvm', 10),
(143, 'Dr. Aida Lockman', 1, '3000.00', 'Voluptatem dignissimos sit rem eaque praesentium. Consequatur et quibusdam laboriosam vel quis quia. Sed magni corrupti magnam sunt quis molestias.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'dr-aida-lockman', 10),
(144, 'Mrs. Maia Maggio MD', 1, '3000.00', 'Et qui mollitia quas quo asperiores. Velit consectetur ullam esse et. Mollitia et rerum est reprehenderit. Optio similique omnis unde.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'mrs-maia-maggio-md', 10),
(145, 'Elsie Fisher', 1, '3000.00', 'In quia voluptate veniam. Corporis omnis blanditiis id dolorem non consequuntur. Non aut est in qui doloremque laudantium.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'elsie-fisher', 10),
(146, 'Dr. Glenda Kuhn', 1, '3000.00', 'Molestiae odio blanditiis ipsa sint mollitia vel. Ad aut deserunt voluptatem sit quia ea maiores. Tempore autem voluptatem et. Ut harum dolore recusandae quaerat quae atque ut.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'dr-glenda-kuhn', 10),
(147, 'Magdalena Dietrich', 1, '3000.00', 'Quia dicta mollitia et inventore. Sunt sed rem aliquid ut. Et nisi animi nihil sit sed. Et harum quisquam laborum aut cum. Dignissimos fugit eaque consectetur architecto amet fugit.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'magdalena-dietrich', 10),
(148, 'Earnest Lesch', 1, '3000.00', 'Doloribus magnam at omnis voluptatem voluptatum officia quam. Ratione ut quidem in voluptatem accusamus.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'earnest-lesch', 10),
(149, 'Prof. Raymundo Franecki MD', 1, '3000.00', 'Ducimus ducimus nostrum non fuga et. Ut sed quia aperiam qui voluptatem qui quisquam. Non mollitia natus necessitatibus neque.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'prof-raymundo-franecki-md', 10),
(150, 'Prof. Aric Morissette', 1, '3000.00', 'Voluptate aliquid libero eum veritatis numquam minus. Dolores facere qui minima sint qui quia voluptate. Eius suscipit quis vel aut.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'prof-aric-morissette', 10),
(151, 'Chaim Schneider', 1, '3000.00', 'Corrupti autem est accusamus tempora aliquam quis. Molestiae voluptatem qui voluptatibus sunt quidem est voluptas. Est autem eum cumque voluptates.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'chaim-schneider', 10),
(152, 'Bette Bradtke', 1, '3000.00', 'Enim earum est blanditiis voluptatum quos non. Reiciendis ad quos sequi hic rem nihil. A facilis eveniet deserunt doloribus fuga. Voluptatem ea est deleniti corrupti.', NULL, '2018-11-13 09:51:13', '2018-11-13 09:51:13', 1, 'bette-bradtke', 10),
(153, 'Mr. Julien Hoppe Jr.', 1, '3000.00', 'Laborum consequatur molestiae nulla labore molestiae. Blanditiis occaecati et veritatis. Omnis vero ea dolor alias. Est accusantium qui voluptas deserunt adipisci.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'mr-julien-hoppe-jr', 10),
(154, 'Harry Greenholt', 1, '3000.00', 'Aspernatur libero quidem dolor provident quis. Esse accusantium dolor dolor sit a quis deserunt. Sunt possimus sit quaerat laudantium.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'harry-greenholt', 10),
(155, 'Deondre Hirthe', 1, '3000.00', 'Doloremque nobis sunt temporibus in esse. Iure eum velit ullam. Omnis dolorem pariatur laborum et.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'deondre-hirthe', 10),
(156, 'Dannie Nicolas', 1, '3000.00', 'Iure cumque veniam voluptatum officia sed ut. Cum recusandae nemo quibusdam. Occaecati quo alias necessitatibus voluptas vel recusandae et. Quia eveniet molestiae et aliquam.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'dannie-nicolas', 10),
(157, 'Dessie Bins', 1, '3000.00', 'Et et nihil nam unde odit et molestias. Enim dolorum voluptas omnis rerum quibusdam at. Saepe et soluta voluptatum et qui. In natus porro qui ea ut cupiditate sed.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'dessie-bins', 10),
(158, 'Darryl Jacobs', 1, '3000.00', 'Alias hic blanditiis ut ut iure assumenda. Voluptatibus et ipsam non dolorem commodi magni qui. Consectetur sed error molestias.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'darryl-jacobs', 10),
(159, 'Sigmund Luettgen IV', 1, '3000.00', 'Sapiente dolorem similique aut illo quia. Quae veritatis dolorem quo consequatur iusto. Animi omnis neque consectetur sed. Qui natus dolorem ab similique sed error eveniet.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'sigmund-luettgen-iv', 10),
(160, 'Mr. Madison Kulas PhD', 1, '3000.00', 'Dolores hic libero non ea. Quo laboriosam nihil maxime porro voluptas. Sit tempora dolorem porro et modi odit voluptatem. Porro nam qui qui et ipsa corrupti commodi.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'mr-madison-kulas-phd', 10),
(161, 'Modesto Terry', 1, '3000.00', 'Vel totam enim ea deleniti suscipit laudantium fugit. Sint qui et sit itaque optio hic nihil nisi. Nobis quisquam reprehenderit mollitia fugit voluptatem.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'modesto-terry', 10),
(162, 'Jalen Reichel', 1, '3000.00', 'Quia dolorem expedita laborum placeat. Consequatur dolor exercitationem numquam sed repellendus nam. Aliquid ea id natus dolorem ratione.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'jalen-reichel', 10),
(163, 'Vernon Huels', 1, '3000.00', 'Corporis dolore maiores quo soluta. Et cupiditate tempore eaque quia in ut. Nulla sit est doloribus repudiandae quod. Asperiores quibusdam aperiam velit ea cum unde ratione ab.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'vernon-huels', 10),
(164, 'Dr. Cydney Bergstrom PhD', 1, '3000.00', 'Ratione ab quaerat ut quis veniam. Numquam ut ex error blanditiis.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'dr-cydney-bergstrom-phd', 10),
(165, 'Quincy Bogan', 1, '3000.00', 'Sit ut facilis dolor totam nihil iure. Nam quia pariatur placeat sit incidunt porro hic et. Qui ut quaerat consequatur reprehenderit deserunt cupiditate accusantium. Quia molestiae autem aut aut.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'quincy-bogan', 10),
(166, 'Dane D\'Amore', 1, '3000.00', 'Alias aut officiis porro et. Dolor laudantium beatae pariatur sapiente tenetur. Quis adipisci repellat quibusdam id ducimus impedit facilis. Dolores modi quo adipisci ipsam at qui.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'dane-damore', 10),
(167, 'Mr. Elian Lebsack MD', 1, '3000.00', 'Mollitia est quia et sit dolore. Cum corporis assumenda voluptates quae. Quia accusamus voluptatum fugiat dolores. Et praesentium vel autem natus.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'mr-elian-lebsack-md', 10),
(168, 'Jayme Aufderhar', 1, '3000.00', 'Ut dicta enim facere consectetur consectetur quaerat. Autem consectetur enim illo dolorem. Commodi illum et tempore voluptatem aut nostrum. Maxime ea quia quam.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'jayme-aufderhar', 10),
(169, 'Dixie Toy', 1, '3000.00', 'Deleniti suscipit illo sit labore provident iusto. Itaque recusandae consequatur rerum error. Perspiciatis id rem delectus odit consequuntur vel.', NULL, '2018-11-13 09:51:14', '2018-11-13 09:51:14', 1, 'dixie-toy', 10),
(170, 'Darrick Goodwin MD', 1, '3000.00', 'Necessitatibus eaque debitis illum consequatur repellendus et in. Inventore at odio autem sunt. Mollitia animi ratione et necessitatibus assumenda sapiente ex.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'darrick-goodwin-md', 10),
(171, 'Katharina Collier', 1, '3000.00', 'Officia dolorum et deserunt reprehenderit saepe quasi vel. Repellat et qui voluptatem nesciunt.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'katharina-collier', 10),
(172, 'Lenny Murray DDS', 1, '3000.00', 'Expedita est quisquam quia vitae eum sit pariatur nisi. Nulla voluptas voluptatem quo eveniet. Voluptate est laborum molestias et. Et aut numquam nostrum atque voluptate qui asperiores.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'lenny-murray-dds', 10),
(173, 'Verlie Raynor', 1, '3000.00', 'Iusto culpa perspiciatis quidem sapiente ut. Optio corporis non consectetur nisi debitis dolorem corporis. Consequatur quibusdam eum in ut ea voluptate dolor.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'verlie-raynor', 10),
(174, 'Dr. Lesly Lemke PhD', 1, '3000.00', 'Eligendi id veniam quibusdam ut nihil sed earum voluptas. Accusantium inventore alias neque aut autem ut. Et similique vero explicabo aut.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'dr-lesly-lemke-phd', 10),
(175, 'Mr. Tyrique Doyle I', 1, '3000.00', 'Facilis porro est sed aut autem dolorem sit. Maxime qui aut autem nesciunt. Mollitia corrupti sapiente deserunt aut suscipit ut sunt nihil. Voluptas vitae assumenda hic. Sit incidunt saepe sit.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'mr-tyrique-doyle-i', 10),
(176, 'Prof. Leta Renner DDS', 1, '3000.00', 'Ullam sunt sed nulla rerum. Qui debitis et id architecto et est et ut.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'prof-leta-renner-dds', 10),
(177, 'Jamil Ziemann Jr.', 1, '3000.00', 'Eveniet architecto totam eligendi modi nulla eum. Rem at culpa optio dolore harum odio dolore.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'jamil-ziemann-jr', 10),
(178, 'Yasmine Konopelski', 1, '3000.00', 'Et rerum eos nisi sapiente quia cumque sit. Explicabo cum iste laborum. Odio corrupti ducimus illo facilis.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'yasmine-konopelski', 10),
(179, 'Osborne Metz', 1, '3000.00', 'Commodi velit incidunt totam. Quia velit soluta quod qui. Sit quaerat aperiam laborum nihil.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'osborne-metz', 10),
(180, 'Rafaela Murphy', 1, '3000.00', 'Quis deserunt voluptas sint quis cumque est voluptate exercitationem. Molestiae laboriosam iure in sit sapiente facilis. Dolor ea et quas sunt. Tempora ad dicta harum natus nam asperiores.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'rafaela-murphy', 10),
(181, 'Dr. Deshaun Blick Sr.', 1, '3000.00', 'Et nobis et atque sed consequatur eos error eos. Quaerat in recusandae laudantium. Et consequatur rerum et eos ut.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'dr-deshaun-blick-sr', 10),
(182, 'Miss Trisha Bauch DDS', 1, '3000.00', 'Dolorem eligendi esse non alias aut et. Molestiae numquam in doloremque velit maxime. Fuga inventore perferendis inventore.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'miss-trisha-bauch-dds', 10),
(183, 'Loy Wolff', 1, '3000.00', 'Fugit nobis consequuntur nihil. Accusamus neque odit debitis sunt distinctio. Totam temporibus corrupti non eos.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'loy-wolff', 10),
(184, 'Kacie Jenkins', 1, '3000.00', 'Eius fugiat laboriosam ipsa ipsam culpa eos totam. Quia dolorem id velit corrupti et sed. Quisquam laudantium aperiam voluptatem.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'kacie-jenkins', 10),
(185, 'Mrs. Macy Adams III', 1, '3000.00', 'Deleniti velit voluptas veniam. Architecto molestiae ut est incidunt maiores quo. Nobis et veniam aut. Voluptatibus quo et ex consequatur et.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'mrs-macy-adams-iii', 10),
(186, 'Corine Harber', 1, '3000.00', 'Placeat nostrum ab architecto a consectetur. Alias est porro nemo magnam ut eum vero cum. Pariatur doloribus eveniet voluptas.', NULL, '2018-11-13 09:51:15', '2018-11-13 09:51:15', 1, 'corine-harber', 10),
(187, 'Mr. Peyton Gislason I', 1, '3000.00', 'In unde ad officiis in explicabo dolorem voluptatibus ipsa. Quis laborum quae quo. Incidunt dolorum sequi architecto debitis sit illum quis. Dolorem autem quos qui.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'mr-peyton-gislason-i', 10),
(188, 'Jazmyn Daniel II', 1, '3000.00', 'Neque sunt in expedita asperiores nam et. Non repellendus quia optio consequuntur. Qui accusamus repellat cum illo quia porro.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'jazmyn-daniel-ii', 10),
(189, 'Reta Kertzmann', 1, '3000.00', 'Reiciendis aspernatur saepe autem asperiores voluptates. Commodi repellendus ipsam et sed sed eos. Est assumenda iure odio beatae incidunt non aliquam.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'reta-kertzmann', 10);
INSERT INTO `products` (`id`, `name`, `category_id`, `price`, `description`, `preview_image_name`, `created_at`, `updated_at`, `is_active`, `slug`, `amount`) VALUES
(190, 'Terrence Emmerich', 1, '3000.00', 'Pariatur vel ducimus deserunt. Labore et rerum ea non alias voluptatem. Et dolores est non veniam. Qui facilis ducimus labore.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'terrence-emmerich', 10),
(191, 'Stacey Gislason MD', 1, '3000.00', 'Tenetur aspernatur voluptatem magnam voluptate enim. Placeat perferendis voluptatum perferendis. Aut omnis minima perferendis culpa vero recusandae dolor enim. Quo sit ut aliquam.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'stacey-gislason-md', 10),
(192, 'Lafayette Kihn', 1, '3000.00', 'Ex et architecto adipisci nihil repellat. Ut ipsa aut eum quia quis. Itaque consequatur totam consequatur eaque autem corrupti voluptatem. Omnis aut similique in ratione quos tempora.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'lafayette-kihn', 10),
(193, 'Prof. Unique Frami PhD', 1, '3000.00', 'Alias numquam incidunt quod itaque nisi molestiae. At reprehenderit qui alias enim. Quidem mollitia et quo corrupti neque et. Esse ea rerum qui laborum recusandae. Ullam necessitatibus ut sequi.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'prof-unique-frami-phd', 10),
(194, 'Cooper Durgan', 1, '3000.00', 'Pariatur voluptatem autem rerum debitis dolor. Expedita voluptas velit adipisci provident. Et ea tempore in placeat voluptate officiis fuga. Illum dolores a quam enim quaerat et.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'cooper-durgan', 10),
(195, 'Chandler Effertz', 1, '3000.00', 'Praesentium ut tempore in aut rem. Est delectus porro possimus quas debitis odio itaque est.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'chandler-effertz', 10),
(196, 'Miss Alysa Bechtelar', 1, '3000.00', 'Rerum laboriosam quo ratione provident atque omnis et. Totam distinctio omnis eligendi ut. Sint nam expedita ut ea voluptatibus.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'miss-alysa-bechtelar', 10),
(197, 'Eda Dicki MD', 1, '3000.00', 'Error est culpa corrupti nobis omnis recusandae architecto. Voluptatem eius laboriosam deleniti placeat optio aut molestiae. Non est autem beatae exercitationem rerum.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'eda-dicki-md', 10),
(198, 'Jonatan Rice', 1, '3000.00', 'Sit voluptatem ea temporibus minima soluta cumque. Dolorem illo sunt adipisci dolorem. Blanditiis incidunt unde sit distinctio. Aut sed doloribus officiis id et.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'jonatan-rice', 10),
(199, 'Brandy Murray', 1, '3000.00', 'Ipsam nihil impedit ut consequuntur saepe rerum neque. Mollitia ut aut vel culpa. Sunt cupiditate et voluptate officiis sequi. Deserunt dolor qui sed quae nemo.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'brandy-murray', 10),
(200, 'Jamel Feil DDS', 1, '3000.00', 'Sit eaque et qui possimus aut. Tenetur atque quam recusandae aut ut ipsa cum. Cupiditate laborum numquam non deserunt aut. Et aut enim sint non sed.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'jamel-feil-dds', 10),
(201, 'Dr. Ladarius Hammes I', 1, '3000.00', 'Placeat aut aut aliquid dolorum voluptatum nihil. Velit corporis quas qui dolore placeat ut. Iste dicta omnis sint nam est aspernatur. Natus id ad culpa cumque.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'dr-ladarius-hammes-i', 10),
(202, 'Mohammad Padberg', 1, '3000.00', 'Itaque nihil aut vel corporis voluptates et a. Sit enim non vel a. Sit quaerat impedit nemo cupiditate asperiores. Ipsa voluptate et iste non rerum quaerat.', NULL, '2018-11-13 09:51:16', '2018-11-13 09:51:16', 1, 'mohammad-padberg', 10),
(203, 'Laila Langosh IV', 1, '3000.00', 'Et fuga et eveniet quisquam fugiat sed. Odio aut earum dolor qui enim est eveniet occaecati. Quod repellat facilis doloribus voluptas nesciunt.', NULL, '2018-11-13 09:51:17', '2018-11-13 09:51:17', 1, 'laila-langosh-iv', 10),
(204, 'Mrs. Nya Sipes DDS', 1, '3000.00', 'Magni ad ducimus qui laborum magni explicabo qui. Ut nulla dolores nam animi eum ut. Quas error nobis sequi aspernatur suscipit. Ea maiores deserunt minima porro aut aspernatur ut.', NULL, '2018-11-13 09:51:17', '2018-11-13 09:51:17', 1, 'mrs-nya-sipes-dds', 10),
(205, 'Sharon Kilback', 1, '3000.00', 'Sunt sint officia dolorem tenetur voluptatem. Minima molestiae dolorem ab nisi itaque. Aliquid nulla provident omnis eum consequatur ut. Fugiat est est consequatur.', NULL, '2018-11-13 09:51:17', '2018-11-13 09:51:17', 1, 'sharon-kilback', 10),
(206, 'Talon Klocko', 1, '3000.00', 'Adipisci aspernatur velit esse quia ab. Quis magnam nihil velit et molestiae nostrum. Unde non eaque adipisci aliquid illum. Aut quis vitae qui dolorem corrupti id.', NULL, '2018-11-13 09:51:17', '2018-11-13 09:51:17', 1, 'talon-klocko', 10),
(207, 'Marcus Little', 1, '3000.00', 'Alias quis molestias perferendis culpa voluptatem earum ad. Nobis unde sapiente qui eos. Accusantium culpa laudantium sit iusto.', NULL, '2018-11-13 09:51:17', '2018-11-13 09:51:17', 1, 'marcus-little', 10),
(208, 'Jamir Hane', 1, '3000.00', 'Est voluptatem voluptates ea sed quis qui facilis. Repudiandae voluptatem ut rem nam. Est eaque optio impedit sequi. Molestias earum pariatur sit voluptatum.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'jamir-hane', 10),
(209, 'Sarah Ebert', 1, '3000.00', 'Deserunt perferendis dolorem fuga eius alias. Placeat alias qui error sit dicta ullam. Harum qui excepturi inventore quo pariatur sit. Sed qui ex harum non. Alias sed aut cupiditate fugit dolor modi.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'sarah-ebert', 10),
(210, 'Dorothy Gislason', 1, '3000.00', 'Aut asperiores in quo aut veniam est. Molestiae aspernatur rerum suscipit fugit esse fugiat atque maiores. Cupiditate quam omnis laborum autem.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'dorothy-gislason', 10),
(211, 'Freddie Bechtelar I', 1, '3000.00', 'Voluptas debitis dolor adipisci asperiores provident. Mollitia neque distinctio natus nostrum. Voluptate corporis enim et eos sunt saepe expedita dicta. Voluptas sunt omnis placeat.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'freddie-bechtelar-i', 10),
(212, 'Calista Upton', 1, '3000.00', 'Dolores ex voluptas inventore dolor quia omnis. In in inventore molestiae sed eos velit. Amet nihil dolores ducimus id expedita non. Excepturi reiciendis maxime perferendis non odio nihil velit.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'calista-upton', 10),
(213, 'Jaylan Hayes Sr.', 1, '3000.00', 'Maiores perferendis nostrum non harum eligendi est. Dolorem nemo quo pariatur reiciendis. Sequi qui ex eos. Et eaque voluptatibus et doloremque harum neque. Modi quas et doloremque velit.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'jaylan-hayes-sr', 10),
(214, 'Antonio Parisian', 1, '3000.00', 'Totam odio aperiam voluptatem officiis quae dolorum minus est. Voluptatem et laborum debitis. Dignissimos qui ut aut. Necessitatibus fugiat et amet quo id totam.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'antonio-parisian', 10),
(215, 'Bruce Rippin', 1, '3000.00', 'Qui ut molestiae quisquam consequuntur. Aliquam voluptas ut perferendis eos sint sequi excepturi provident.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'bruce-rippin', 10),
(216, 'Prof. Zack Kuhic', 1, '3000.00', 'Ipsa officiis quia nostrum nihil soluta commodi iure qui. Tempore consequatur ut id incidunt. Aut et error fuga quo repudiandae mollitia accusantium.', NULL, '2018-11-13 09:51:23', '2018-11-13 09:51:23', 1, 'prof-zack-kuhic', 10),
(217, 'Miss Esta Muller III', 1, '3000.00', 'Cumque et laudantium saepe numquam esse. Harum sint eveniet et consequatur officiis ea. Pariatur est facilis aliquid nostrum eveniet libero.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'miss-esta-muller-iii', 10),
(218, 'Mrs. Emmie Macejkovic', 1, '3000.00', 'Voluptates impedit laudantium laudantium. Ut ut sunt nostrum explicabo. Nobis quia molestiae saepe qui quo explicabo similique. Est molestias est adipisci.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'mrs-emmie-macejkovic', 10),
(219, 'Sage Rogahn', 1, '3000.00', 'Corporis animi reiciendis fugit error et aut enim consequatur. Eius impedit deleniti inventore molestiae ex atque voluptatum ullam.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'sage-rogahn', 10),
(220, 'Prof. Felicity Funk', 1, '3000.00', 'Ea qui provident non quis dolorum aut. Voluptatem doloribus deleniti cupiditate delectus nihil. Qui cum natus quia. Eos molestiae incidunt inventore eum.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'prof-felicity-funk', 10),
(221, 'River Langworth', 1, '3000.00', 'Voluptatem hic consequuntur est ipsa maxime libero. A dolore ab ipsam pariatur aliquam consectetur illo. Illo autem alias sit sequi et. Nam sit voluptas aut rerum et.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'river-langworth', 10),
(222, 'Kayli Reinger', 1, '3000.00', 'Dolor et culpa ullam voluptatem. Pariatur aut magni veniam veritatis ducimus eveniet consequatur culpa. Pariatur sed repellat corrupti veniam similique omnis odit.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'kayli-reinger', 10),
(223, 'Brisa Kunze', 1, '3000.00', 'Quaerat tempora accusantium nemo quibusdam quas sit. Quidem ipsum quo rerum sed laboriosam. Consequatur nihil cumque ipsam voluptatem sit ut. Eaque sit aut at accusantium.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'brisa-kunze', 10),
(224, 'Michel Bradtke', 1, '3000.00', 'Ut sint odio provident a cumque deserunt. Temporibus dignissimos enim beatae et deleniti. Iste a vero quaerat assumenda. Debitis quidem nulla non. Aliquid est omnis error quia voluptas.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'michel-bradtke', 10),
(225, 'Genoveva Wisoky', 1, '3000.00', 'Dolor quia delectus impedit veniam. Et illo dicta est unde ipsam eos. Placeat explicabo ea reiciendis voluptatem fuga voluptatem voluptatum.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'genoveva-wisoky', 10),
(226, 'Polly Powlowski', 1, '3000.00', 'Sunt voluptas ipsam perferendis et consequuntur impedit. Nemo aut accusantium ipsam qui. Totam doloremque quia voluptas ratione aliquam consequatur omnis. Est non quo excepturi rem asperiores.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'polly-powlowski', 10),
(227, 'Dr. Destinee Treutel', 1, '3000.00', 'Explicabo architecto voluptas eos quidem. Aliquid pariatur ut eum eos rerum quibusdam sit. Accusamus ut unde est voluptatem et eveniet quaerat quis. Et quos incidunt neque.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'dr-destinee-treutel', 10),
(228, 'Kelton Veum', 1, '3000.00', 'Sint laboriosam sit exercitationem enim sit nam eveniet. Et veritatis temporibus eius sed laudantium sed sit quibusdam. Ab omnis alias voluptate sed velit illo. Quia autem modi cumque.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'kelton-veum', 10),
(229, 'Prof. Emilie Hills III', 1, '3000.00', 'Itaque enim blanditiis expedita voluptates ut. Consequatur consequuntur debitis veritatis. Libero odit exercitationem molestiae dolores et. Optio omnis quas deserunt ad.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'prof-emilie-hills-iii', 10),
(230, 'Buford Fadel', 1, '3000.00', 'Reiciendis voluptas officia sunt. Repellat sequi et dolore vero maxime placeat illo. Molestiae quasi odit ratione. Perspiciatis optio sed sit non eveniet.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'buford-fadel', 10),
(231, 'Dr. Brooklyn Lynch Sr.', 1, '3000.00', 'Eligendi et nisi vel autem reiciendis nulla unde. Ex voluptates minus saepe ratione. Tenetur qui ipsam molestiae ratione corrupti et. Eum quos quam et facere sunt sequi magni.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'dr-brooklyn-lynch-sr', 10),
(232, 'Kraig Tillman', 1, '3000.00', 'Qui dolorem est qui qui eos dolores. Facilis voluptates vel animi rerum harum sed iure. Ad repellendus beatae voluptates consequatur odio. Fuga amet incidunt fugiat voluptas vero ut.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'kraig-tillman', 10),
(233, 'Ms. Amelie Jacobs', 1, '3000.00', 'Sint facilis dicta laboriosam eius. Nihil dolores veniam laborum cum. Alias qui ipsam vel aut natus aliquam quisquam. Magnam iusto dolorem animi molestiae quibusdam. Nesciunt rem aut error aut ut.', NULL, '2018-11-13 09:51:24', '2018-11-13 09:51:24', 1, 'ms-amelie-jacobs', 10),
(234, 'Reyes Nolan', 1, '3000.00', 'Veritatis recusandae deserunt rerum ea. Voluptatem explicabo sit consequatur quis aut porro numquam. Odit aliquam aut numquam quibusdam voluptas.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'reyes-nolan', 10),
(235, 'Kelly Strosin III', 1, '3000.00', 'Natus sed laborum voluptate consequuntur et nisi est. Minima ullam odit porro dolor. Maxime aut consequatur est eligendi aliquam iusto vitae.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'kelly-strosin-iii', 10),
(236, 'Allan McKenzie', 1, '3000.00', 'Dolor nemo aut velit voluptas dolorum qui et ipsam. Cum impedit voluptatem repellendus doloremque cumque laudantium et.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'allan-mckenzie', 10),
(237, 'Marina Heaney', 1, '3000.00', 'Sunt nulla occaecati facilis. Est et quos blanditiis et aut. Debitis autem tenetur temporibus in commodi. Ut fugit et error velit et nam perferendis illum.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'marina-heaney', 10),
(238, 'Alysson Schuster', 1, '3000.00', 'Commodi quia similique deleniti libero dicta cum. Saepe incidunt accusamus laboriosam quaerat. Earum aspernatur perferendis iure velit est optio at aut. Fugiat voluptatem at nam doloribus.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'alysson-schuster', 10),
(239, 'Dr. Quinton Mosciski', 1, '3000.00', 'Quis sequi consequuntur vel minima consectetur. Qui voluptatum non ipsum quia voluptatum praesentium. Et perferendis quibusdam et quia.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'dr-quinton-mosciski', 10),
(240, 'Sarah Beier', 1, '3000.00', 'Et tempora quam quas aperiam. Quos tempore assumenda voluptas perspiciatis et sit nam. Eos voluptatibus veritatis libero.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'sarah-beier', 10),
(241, 'Mrs. Callie Leffler Sr.', 1, '3000.00', 'Dolor qui cumque nemo doloremque ex sit et unde. Enim itaque enim sed nam aut ipsum quam. Sed modi vel et eos architecto alias sed.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'mrs-callie-leffler-sr', 10),
(242, 'Prof. Janiya Towne DVM', 1, '3000.00', 'Consequatur tenetur similique accusantium aut quo. Quaerat nulla veniam qui voluptate. Consequatur perspiciatis esse ea delectus illum.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'prof-janiya-towne-dvm', 10),
(243, 'Sienna Christiansen II', 1, '3000.00', 'Repellat laboriosam eaque perferendis quod tempore est. Magni rerum deserunt et et. Aperiam unde eum eos.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'sienna-christiansen-ii', 10),
(244, 'Heather Predovic', 1, '3000.00', 'Explicabo id saepe doloribus quis. Qui quis eum voluptas rem asperiores ut. Dolore deserunt molestiae natus natus quis dolorem dignissimos. Est odio rerum atque veritatis voluptatem.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'heather-predovic', 10),
(245, 'Trevion Stehr', 1, '3000.00', 'Tempora modi unde deleniti excepturi hic. Ad ut quibusdam natus nobis assumenda. Sed aliquam magni et eum in quidem maxime. Unde sed veniam quia quas.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'trevion-stehr', 10),
(246, 'Abigale Ratke', 1, '3000.00', 'Dolorem enim animi sapiente quia. Possimus est omnis quidem aperiam eveniet. Accusantium aut numquam est.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'abigale-ratke', 10),
(247, 'Mrs. Amira Little', 1, '3000.00', 'Ut qui necessitatibus mollitia dolorem quo consequatur. Quae sunt a omnis aut asperiores. Id quia et officiis suscipit nostrum. Eligendi non delectus sunt est.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'mrs-amira-little', 10),
(248, 'Leone Lakin', 1, '3000.00', 'Numquam voluptatem accusamus voluptas commodi et quo deserunt. Pariatur et quis similique consequatur est. Praesentium quis et veniam earum voluptatem. Repudiandae odit non dolor.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'leone-lakin', 10),
(249, 'Ms. Alene Nitzsche I', 1, '3000.00', 'Quis dolorem vel quia iste. Voluptas vel quisquam et eum quia ipsum. Voluptatem nobis libero dolore unde nemo esse. Est nisi et voluptatum.', NULL, '2018-11-13 09:51:25', '2018-11-13 09:51:25', 1, 'ms-alene-nitzsche-i', 10),
(250, 'Jewell Tillman', 1, '3000.00', 'Voluptates quo voluptas qui reiciendis tempora cupiditate. Dolores quae voluptas nam alias et. Dicta veritatis et est facilis nobis enim.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'jewell-tillman', 10),
(251, 'Celestine Witting', 1, '3000.00', 'Laudantium quod eaque quo quasi sint. Aut cumque et excepturi sapiente earum et eos. Quia officia molestiae nobis id. Nesciunt et necessitatibus quis eum.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'celestine-witting', 10),
(252, 'Noemy Hoeger', 1, '3000.00', 'Suscipit sint voluptatem possimus repudiandae est nam. Officiis et animi accusamus est voluptas. Quidem quod ut deserunt ea nesciunt reiciendis.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'noemy-hoeger', 10),
(253, 'Jackeline Langosh', 1, '3000.00', 'Ad harum aut voluptates quo. In ducimus qui vel ut qui. Ut sint et animi qui ea. Nihil vero voluptas dolores ut eius sint iure.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'jackeline-langosh', 10),
(254, 'Fredrick Block', 1, '3000.00', 'Aut saepe ratione eius sint. Eos soluta quia et ex explicabo aut sed. Eaque numquam qui dolore occaecati nulla sed qui. Eius et dolores qui officiis id est possimus.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'fredrick-block', 10),
(255, 'Emil McGlynn II', 1, '3000.00', 'Qui numquam adipisci ut. Consequatur et minus deleniti quidem animi in fugiat. Corrupti facere ut aut et.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'emil-mcglynn-ii', 10),
(256, 'Miss Ona Mante V', 1, '3000.00', 'Temporibus sit omnis rerum qui. Atque quas consequuntur ut alias quia. Blanditiis veritatis voluptas distinctio. Numquam eum sed earum vel. Veniam recusandae totam quas sunt illum laborum omnis.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'miss-ona-mante-v', 10),
(257, 'Kiana Kautzer', 1, '3000.00', 'Odit totam qui sint reprehenderit soluta laboriosam. Aut quia voluptates maxime aspernatur ipsam. Quae et numquam quibusdam eum ut ratione eius.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'kiana-kautzer', 10),
(258, 'Ellie Pouros', 1, '3000.00', 'Placeat sit quia ex. Earum ut repellat nihil esse ut eum. Dolore itaque sapiente vitae non esse dicta accusamus occaecati.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'ellie-pouros', 10),
(259, 'Caden Rippin Jr.', 1, '3000.00', 'Et in aspernatur delectus nemo maxime deserunt labore quisquam. Dolores aut et eveniet dignissimos dolores. Sunt nam et aspernatur commodi repellendus omnis.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'caden-rippin-jr', 10),
(260, 'Dessie Kassulke', 1, '3000.00', 'Modi voluptate impedit neque et eum provident. Omnis et voluptatum molestiae praesentium aut. Ducimus beatae consequatur excepturi iusto voluptates maxime. Assumenda esse nihil soluta sit.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'dessie-kassulke', 10),
(261, 'Greta Schaden', 1, '3000.00', 'Itaque qui consequatur qui non eos ea assumenda. Quia unde iusto ad officia est et. Eum unde dolores quis accusantium ipsa rerum.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'greta-schaden', 10),
(262, 'Odell Ziemann', 1, '3000.00', 'Ratione quibusdam quidem omnis voluptas quia. Id doloribus omnis eius.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'odell-ziemann', 10),
(263, 'Aaliyah Torphy II', 1, '3000.00', 'Et labore error provident rem dolores sint tenetur nam. Impedit qui sit ducimus inventore numquam. Itaque voluptate et iste ipsum expedita et.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'aaliyah-torphy-ii', 10),
(264, 'Alberta Kuhlman', 1, '3000.00', 'Aliquam omnis occaecati dolor asperiores in. Sunt et neque voluptates similique temporibus possimus.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'alberta-kuhlman', 10),
(265, 'Nicholas Pagac', 1, '3000.00', 'Deleniti accusamus sed quo quas qui sit. Et accusamus ea vel esse magni a. Ipsa temporibus iusto id sit ducimus. Aut quo aut autem dolore id.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'nicholas-pagac', 10),
(266, 'Theodora Watsica', 1, '3000.00', 'Vel eum voluptatem temporibus tempora blanditiis et. Eum impedit doloremque tempore nostrum ut. Sit quo ad expedita et rerum esse iure.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'theodora-watsica', 10),
(267, 'Prof. Lina Borer DVM', 1, '3000.00', 'Sit necessitatibus illo aut maiores labore. Voluptatem voluptas distinctio aspernatur aut vitae quas. Soluta aut itaque nostrum omnis aut.', NULL, '2018-11-13 09:51:26', '2018-11-13 09:51:26', 1, 'prof-lina-borer-dvm', 10),
(268, 'Dominique Dibbert', 1, '3000.00', 'Beatae sed a nesciunt. Sint quod consequuntur vitae laudantium rem laudantium. Voluptas aut vel ut molestiae. Sed quia ullam tempore corporis.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'dominique-dibbert', 10),
(269, 'April Boehm', 1, '3000.00', 'Fugiat nostrum et reiciendis inventore aut quas. Ea esse sequi magnam labore. Eos ipsum reiciendis autem et molestiae. Et qui vero delectus aliquam.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'april-boehm', 10),
(270, 'Sim Beahan', 1, '3000.00', 'Mollitia est ratione iure quas. Ipsa modi expedita cupiditate. Asperiores voluptate non voluptatem quasi unde consequatur. Accusamus quis consequuntur quod fugit ut odio maiores.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'sim-beahan', 10),
(271, 'Heidi Ruecker', 1, '3000.00', 'Sit corrupti mollitia temporibus. Adipisci molestiae quae quis illum unde minus. Et ab ab ipsa. Itaque eligendi soluta eius beatae quo quasi corrupti.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'heidi-ruecker', 10),
(272, 'Arlo Feil', 1, '3000.00', 'Assumenda sunt suscipit unde pariatur consequatur doloremque. At deserunt necessitatibus omnis. Eos sequi quaerat repudiandae quis debitis aperiam quam.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'arlo-feil', 10),
(273, 'Shakira Stamm MD', 1, '3000.00', 'Qui et eius nihil eveniet labore asperiores cum. Dolores impedit minus rerum incidunt. Dolore sunt repellendus inventore dolores laborum hic est.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'shakira-stamm-md', 10),
(274, 'Chaim Ziemann', 1, '3000.00', 'Rerum et vero temporibus sit dicta aut. Quaerat incidunt quaerat suscipit quia cupiditate voluptate. Ipsum molestiae ut est ut distinctio. Nihil omnis doloremque et veritatis similique est porro.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'chaim-ziemann', 10),
(275, 'Moises Marquardt MD', 1, '3000.00', 'Explicabo voluptas enim assumenda quasi veniam sit dolore. Earum est omnis vero omnis. Ut asperiores facere nesciunt nulla non voluptatem.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'moises-marquardt-md', 10),
(276, 'Anderson Hegmann', 1, '3000.00', 'Dignissimos voluptas voluptas ut. Facilis et sed ducimus facilis. Laudantium blanditiis sed at odio nihil quia.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'anderson-hegmann', 10),
(277, 'Johann Connelly', 1, '3000.00', 'Rem corrupti consequatur dicta debitis. Facere necessitatibus deserunt nihil voluptatem qui voluptate optio dolorum. Aliquam enim cupiditate dolor qui voluptatum sed. Deleniti esse in et sed.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'johann-connelly', 10),
(278, 'Elissa Murazik MD', 1, '3000.00', 'Laboriosam pariatur odio aut animi sequi. Deserunt voluptatibus nesciunt id. Tempora eos nihil omnis quod ut facilis.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'elissa-murazik-md', 10),
(279, 'Prof. Mohamed Daniel', 1, '3000.00', 'Et maiores ad deserunt doloribus ex et natus. Quo accusamus consequatur velit maxime. A recusandae porro non voluptatem numquam laboriosam qui. Nisi perspiciatis provident cupiditate numquam.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'prof-mohamed-daniel', 10),
(280, 'Kenna Daugherty', 1, '3000.00', 'Quaerat voluptas expedita enim. Pariatur aperiam nulla sint ullam. Enim asperiores accusantium deleniti inventore et explicabo.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'kenna-daugherty', 10),
(281, 'Oceane Ritchie', 1, '3000.00', 'Omnis aut aut aut dolore et. Laudantium fugit voluptatibus sint officiis. Ut consequuntur nulla facere rerum dolor. Rem et praesentium deleniti non eum corrupti est.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'oceane-ritchie', 10),
(282, 'Montana Reinger', 1, '3000.00', 'Hic impedit labore tempore sapiente laudantium non. Perspiciatis commodi qui aut id. Consequuntur quod qui enim eum earum minus.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'montana-reinger', 10),
(283, 'Tina Farrell MD', 1, '3000.00', 'Quia eius possimus ad. Similique recusandae eligendi sunt maiores laboriosam nihil. Dignissimos et fugit et perferendis.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'tina-farrell-md', 10),
(284, 'Lilliana Botsford', 1, '3000.00', 'Rerum inventore cumque pariatur nostrum ratione voluptatum. Id quod sapiente est aut. Neque reiciendis occaecati molestiae officiis illum. Quasi cumque corrupti nesciunt necessitatibus nulla non.', NULL, '2018-11-13 09:51:27', '2018-11-13 09:51:27', 1, 'lilliana-botsford', 10),
(285, 'Alexys Orn DVM', 1, '3000.00', 'Voluptas aliquid corporis et enim at optio aliquid. Hic modi quo ut voluptatem sit at. Nesciunt nisi exercitationem quia ut id nam nemo. Similique sequi ratione veniam possimus.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'alexys-orn-dvm', 10),
(286, 'Annabel Carter', 1, '3000.00', 'Maiores aut in accusamus pariatur vero. Corrupti porro et consequatur necessitatibus quos enim eaque. Et optio error ut quaerat.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'annabel-carter', 10),
(287, 'Letha Herman', 1, '3000.00', 'Provident atque autem laudantium voluptatibus dolores alias sequi. Est ducimus ut unde sit et unde. Necessitatibus et ut accusantium temporibus. Modi aut assumenda qui.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'letha-herman', 10),
(288, 'Gianni Harris', 1, '3000.00', 'Possimus fugit sunt enim tempora aut omnis. Ab molestiae alias fuga laudantium. Recusandae ut libero earum rerum beatae aspernatur.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'gianni-harris', 10),
(289, 'Jon Macejkovic', 1, '3000.00', 'Consequuntur rem voluptatem odio consequatur repellat quis. Esse sed recusandae quas doloribus quod illo.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'jon-macejkovic', 10),
(290, 'Augustus Douglas', 1, '3000.00', 'Sint occaecati libero consectetur sunt quae. Ipsum eos voluptatem ipsa eos ut. Debitis ut aperiam quos velit et.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'augustus-douglas', 10),
(291, 'Reid Gislason', 1, '3000.00', 'Quis rerum dolore aut vero dolor. Adipisci quia aut omnis qui debitis pariatur neque. Commodi praesentium eum omnis soluta.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'reid-gislason', 10),
(292, 'Janiya Klein', 1, '3000.00', 'In aut rerum cumque illum. Quis a cum delectus quidem. Itaque veritatis animi eos ea. Ducimus ducimus qui natus ratione quisquam.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'janiya-klein', 10),
(293, 'Mrs. Oma Halvorson DVM', 1, '3000.00', 'Et aliquam recusandae magni quasi rerum. Doloremque voluptatum sint asperiores harum quia earum. Qui minima debitis qui minus in dignissimos possimus voluptatum.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'mrs-oma-halvorson-dvm', 10),
(294, 'Miss Juana Cormier PhD', 1, '3000.00', 'Est laborum alias rerum reprehenderit qui. Culpa ut veritatis qui aperiam sit dolor. Molestiae odit aut id est quia laborum et.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'miss-juana-cormier-phd', 10),
(295, 'Austyn Schneider', 1, '3000.00', 'Ea incidunt nobis dolorem sit quas. Veritatis ea qui rem sit itaque et consequuntur. Minus vero voluptatem beatae vel sed.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'austyn-schneider', 10),
(296, 'Cristopher Hoppe', 1, '3000.00', 'Veniam corrupti ratione omnis quidem dolores. Earum quia placeat delectus adipisci tenetur possimus. Accusamus autem adipisci quas rerum ducimus ratione.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'cristopher-hoppe', 10),
(297, 'Camila Herzog', 1, '3000.00', 'Provident ea possimus autem cupiditate ipsa. Sunt omnis expedita doloribus. Minus dolores vel ducimus illo unde velit voluptatem. Ipsum modi hic reprehenderit assumenda.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'camila-herzog', 10),
(298, 'Amos McLaughlin', 1, '3000.00', 'Accusantium est pariatur quis hic sed dolorum. Voluptatem quasi dignissimos natus ut est est aut. Ducimus qui ratione et sit. Et molestiae omnis excepturi non harum saepe et.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'amos-mclaughlin', 10),
(299, 'Prof. Delaney Carter Jr.', 1, '3000.00', 'Et ipsa repellendus sint explicabo ut soluta saepe. Odio cumque nobis magni quae rerum. Sequi qui maiores quia accusantium at minima occaecati eum.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'prof-delaney-carter-jr', 10),
(300, 'Mrs. Malvina Fay IV', 1, '3000.00', 'Consequatur id vel earum iure cumque qui. Aut reiciendis officiis nemo repudiandae fuga iusto incidunt. Sit quos hic alias nostrum.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'mrs-malvina-fay-iv', 10),
(301, 'Rachel Hoeger Sr.', 1, '3000.00', 'Similique ut quia ipsa pariatur voluptatem iusto quam. Fugit eaque et nemo eligendi in eos. Eligendi in placeat rem.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'rachel-hoeger-sr', 10),
(302, 'Prof. Kennedi McLaughlin DDS', 1, '3000.00', 'Sunt molestiae necessitatibus veniam repellendus dicta. Fugiat qui qui enim ut. Magni aliquid quibusdam iusto recusandae rem atque et. Distinctio aliquid dolorem ut provident velit suscipit enim.', NULL, '2018-11-13 09:51:28', '2018-11-13 09:51:28', 1, 'prof-kennedi-mclaughlin-dds', 10),
(303, 'Dr. Ervin Davis', 1, '3000.00', 'Aut amet doloribus quo incidunt quidem voluptatem et. Non ex aspernatur voluptatem ea nihil ex quis. Repudiandae cum nam iure ea aut dolore tenetur. Vel labore nesciunt voluptas doloribus.', NULL, '2018-11-13 09:51:29', '2018-11-13 09:51:29', 1, 'dr-ervin-davis', 10),
(304, 'Destiny Aufderhar', 1, '3000.00', 'Rerum magni quis facere. Nostrum aut molestiae consequuntur quasi aut provident incidunt aut. Ducimus nihil asperiores esse.', NULL, '2018-11-13 09:51:29', '2018-11-13 09:51:29', 1, 'destiny-aufderhar', 10),
(305, 'Emie O\'Connell MD', 1, '3000.00', 'Dignissimos vel placeat hic et voluptatum. Saepe fugit consequatur consequatur odit et earum in. Voluptatem voluptas est delectus eligendi quam suscipit.', NULL, '2018-11-13 09:51:29', '2018-11-13 09:51:29', 1, 'emie-oconnell-md', 10),
(306, 'Katharina Koelpin', 1, '3000.00', 'Ea adipisci molestias ducimus et deserunt adipisci eum sed. Cumque dignissimos nisi qui atque quia ut vero. Harum in in eum sit veniam. Eligendi ex consequuntur enim natus dolorem quis.', NULL, '2018-11-13 09:51:29', '2018-11-13 09:51:29', 1, 'katharina-koelpin', 10),
(307, 'Aida Bahringer', 1, '3000.00', 'Unde dicta ullam nam rerum architecto recusandae reiciendis. Neque consequatur voluptas nam sint. Culpa ab corrupti laudantium.', NULL, '2018-11-13 09:51:29', '2018-11-13 09:51:29', 1, 'aida-bahringer', 10),
(308, 'Christa Dooley', 1, '3000.00', 'Illum mollitia iste ut ea amet iure. Rerum unde quia laborum sapiente voluptas saepe eum.', NULL, '2018-11-13 09:51:29', '2018-11-13 09:51:29', 1, 'christa-dooley', 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password_hash`, `created_at`, `updated_at`, `auth_key`, `access_token`) VALUES
(6, 'admin', '$2y$13$ZEcXASIZv2rDH30YKIY3Ae/P2oQplZhDCpYKDZYJdDEinMgd.N9du', '2018-11-13 10:04:05', '2018-11-13 10:04:05', 'nm5qpo4CJuQMAd4iw_AHbQ0p161BG7rw', 'Q3aCEPFGfafgoNLroM6QChVMr242CwKf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `favorite_products`
--
ALTER TABLE `favorite_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `favorite_products`
--
ALTER TABLE `favorite_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=309;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorite_products`
--
ALTER TABLE `favorite_products`
  ADD CONSTRAINT `favorite_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorite_products_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
