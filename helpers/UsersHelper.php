<?php

namespace app\helpers;

use Yii;
use Exception;
use app\models\Users;
use yii\base\Component;
use app\forms\LoginForm;
use app\forms\RegisterForm;
use yii\web\NotFoundHttpException;
use app\forms\UserForm;

/**
 * Description of UsersHelper
 *
 * @author mezinn
 */
class UsersHelper extends Component {

    public function createNew(UserForm $form) {
        return Yii::$app->getDb()->transaction(function()use($form) {
                    $user = new Users([
                        'username' => $form->getUsername(),
                        'password_hash' => Yii::$app->security->generatePasswordHash($form->password),
                        'auth_key' => Yii::$app->security->generateRandomString(),
                        'access_token' => Yii::$app->security->generateRandomString(),
                    ]);
                    return $user->save() and Yii::$app->authManager->assign(Yii::$app->authManager->getRole(CUSTOMER), $user->id);
                });
    }

    public function login(UserForm $form) {
        try {
            if (Yii::$app->security->validatePassword($form->getPassword(), $this->findIdentityByUsername($form->getUsername())->password_hash)) {
                return Yii::$app->user->login($this->findIdentityByUsername($form->getUsername()));
            }
        } catch (Exception $ex) {
            
        }
        Yii::$app->session->setFlash('error', 'Username or password incorrect.');
        return false;
    }

    public function findIdentityByUsername(string $username) {
        if (($model = Users::findOne(['username' => $username]))) {
            return $model;
        }
        throw new NotFoundHttpException();
    }

    public function getRegisterForm() {
        return new RegisterForm();
    }

    public function getLoginForm() {
        return new LoginForm();
    }

}
