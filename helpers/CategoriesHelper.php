<?php

namespace app\helpers;

use yii\base\Component;
use app\models\Categories;
use yii\helpers\ArrayHelper;

/**
 * Description of CategoriesHelper
 *
 * @author mezinn
 */
class CategoriesHelper extends Component {

    public function getActiveList() {
        return ArrayHelper::map(Categories::find()->where(['is_active' => true])->all(), 'id', 'name');
    }

}
