<?php

namespace app\helpers;

use Yii;
use yii\base\Component;
use app\models\FavoriteProducts;

/**
 * Description of ProductsHelper
 *
 * @author mezinn
 */
class ProductsHelper extends Component {

    public function toggleFavorite($id) {
        Yii::error(['here1']);

        if (($model = FavoriteProducts::findOne(['product_id' => $id, 'user_id' => Yii::$app->user->id]))) {
            Yii::error(['here']);
            return $model->delete();
        }
        return (new FavoriteProducts(['product_id' => $id, 'user_id' => Yii::$app->user->id]))->save();
    }

    public function isFavorite($id) {
        return FavoriteProducts::findOne(['product_id' => $id, 'user_id' => Yii::$app->user->id]) !== null;
    }

}
