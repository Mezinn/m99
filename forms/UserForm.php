<?php

namespace app\forms;

interface UserForm  {

    public function getUsername();

    public function getPassword();
}
