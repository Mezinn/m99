<?php

namespace app\forms;

use yii\base\Model;

class LoginForm extends Model implements UserForm {

    public $username;
    public $password;

    public function rules() {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'string'],
        ];
    }

    public function getPassword() {
        return $this->password;
    }

    public function getUsername() {
        return $this->username;
    }

}
