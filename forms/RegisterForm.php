<?php

namespace app\forms;

use yii\base\Model;
use app\models\Users;

class RegisterForm extends Model implements UserForm {

    public $username;
    public $password;
    public $confirm_password;

    public function rules() {
        return [
            [['username', 'password', 'confirm_password'], 'required'],
            [['username', 'password', 'confirm_password'], 'string', 'max' => 255],
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
            [['username'], 'unique', 'targetClass' => Users::className()]
        ];
    }

    public function getPassword() {
        return $this->password;
    }

    public function getUsername() {
        return $this->username;
    }

}
