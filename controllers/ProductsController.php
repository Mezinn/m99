<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use app\models\search\ProductsOuterSearch;

/**
 * Description of ProductControllers
 *
 * @author mezinn
 */
class ProductsController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['favorites'],
                'rules' => [
                    [
                        'actions' => ['favorites'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new ProductsOuterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionToggleFavorite($id) {
        Yii::error('controller');
        return Yii::$app->productsHelper->toggleFavorite($id);
    }

    public function actionFavorites() {
        return $this->render('favorites', ['dataProvider' => Yii::$app->user->identity->getFavoritesDataProvider()]);
    }

}
