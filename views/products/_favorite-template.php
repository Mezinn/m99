<?php

use yii\helpers\StringHelper;
?>

<div class="thumbnail">
    <img src="<?= "/{$model->product->preview_image_path}" ?>" alt="...">
    <div class="caption">
        <h5 title="<?= $model->product->name ?>"><?= StringHelper::truncate($model->product->name, 30) ?></h5>
        <p><?= intval($model->product->price) ?></p>
        <p><a class="btn btn-danger" onclick="toggle_favorite(event)" data-id ="<?= $model->product->id ?>">Видалити</a></p>
    </div>
</div>

