<?php

use yii\helpers\StringHelper;
?>

<div class="thumbnail">
    <img src="<?= "/$model->preview_image_path" ?>" alt="...">
    <div class="caption">
        <h5 title="<?= $model->name ?>"><?= StringHelper::truncate($model->name, 30) ?></h5>
        <p><?= intval($model->price) ?></p>
        <?php if (Yii::$app->productsHelper->isFavorite($model->id)): ?>
            <p><a class="btn btn-danger" onclick="toggle_favorite(event)" data-id ="<?= $model->id ?>">Видалити</a></p>
        <?php else: ?>
            <p><a class="btn btn-default" onclick="toggle_favorite(event)" data-id ="<?= $model->id ?>">Зберегти</a></p>
        <?php endif; ?>
    </div>
</div>

