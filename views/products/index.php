<?php

use yii\widgets\ListView;
?>


<?php yii\widgets\Pjax::begin(['id' => 'products']) ?>
<div class="row">
    <?=
    $this->render('/manage/products/_search', [
        'model' => $searchModel,
        'content' => ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_template',
            'options' => ['class' => 'products-list'],
            'itemOptions' => ['class' => 'product__thumb'],
])])
    ?>
</div>
<?php yii\widgets\Pjax::end() ?>
<script>
    function toggle_favorite(event) {
        event.preventDefault();
        $.ajax({
            url: '/products/toggle-favorite',
            data: {id: event.target.dataset.id},
            success: function (data, textStatus, jqXHR) {
                $.pjax.reload({container: "#products"});

            }
        });
    }
</script>