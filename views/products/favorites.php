<?php

use yii\widgets\ListView;
?>


<?php yii\widgets\Pjax::begin(['id' => 'favorites']); ?>
<div class="row">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_favorite-template',
        'options' => ['class' => 'products-list'],
        'itemOptions' => ['class' => 'product__thumb'],
    ])
    ?>

</div>
<?php yii\widgets\Pjax::end(); ?>
<script>
    function toggle_favorite(event) {
        event.preventDefault();
        $.ajax({
            url: '/products/toggle-favorite',
            data: {id: event.target.dataset.id},
            success: function (data, textStatus, jqXHR) {
                $.pjax.reload({container: "#favorites"});

            }
        });

    }
</script>