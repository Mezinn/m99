<aside class="main-sidebar">

    <section class="sidebar">
        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => 'Категорії', 'icon' => 'trello', 'url' => ['/manage/categories']],
                        ['label' => 'Товари', 'icon' => 'th-large', 'url' => ['/manage/products']],
                        ['label' => 'Користувачі', 'icon' => 'user', 'url' => ['/manage/users']],
                        ['label' => 'Вихід', 'icon' => 'sign-out', 'url' => ['/site/logout'], 'options' => ['data-method' => 'post']],
                    ],
                ]
        )
        ?>

    </section>

</aside>
