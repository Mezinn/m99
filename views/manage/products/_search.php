<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="products-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1
                ],
    ]);
    ?>

    <div class="row">
        <div class="col-md-11">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label>&#8203;</label>
                <?= Html::submitButton('Пошук', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'category_id')->dropDownList(Yii::$app->categoriesHelper->getActiveList()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput() ?>

        </div>

        <?= $content ?>


    </div>
    <?php ActiveForm::end(); ?>
</div>